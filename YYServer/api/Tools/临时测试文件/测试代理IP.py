import requests
import random
import os
result= [
    {
        "city_cn": "四川省德阳市",
        "host": "61.188.233.151",
        "port": "50458"
    },
    {
        "city_cn": "浙江省",
        "host": "183.133.65.250",
        "port": "51644"
    },
    {
        "city_cn": "江苏省淮安市",
        "host": "114.238.39.102",
        "port": "42091"
    },
    {
        "city_cn": "福建省漳州市",
        "host": "218.67.31.200",
        "port": "34044"
    },
    {
        "city_cn": "辽宁省",
        "host": "42.179.168.118",
        "port": "35317"
    },
    {
        "city_cn": "辽宁省鞍山市",
        "host": "60.17.255.169",
        "port": "55606"
    },
    {
        "city_cn": "湖北省",
        "host": "171.40.164.105",
        "port": "40276"
    },
    {
        "city_cn": "湖北省仙桃市",
        "host": "116.209.54.166",
        "port": "59477"
    },
    {
        "city_cn": "福建省泉州市",
        "host": "120.43.58.136",
        "port": "35029"
    },
    {
        "city_cn": "福建省南平市",
        "host": "27.150.35.48",
        "port": "48305"
    },
    {
        "city_cn": "湖北省随州市",
        "host": "116.208.201.49",
        "port": "48978"
    },
    {
        "city_cn": "江西省鹰潭市",
        "host": "59.62.113.197",
        "port": "37032"
    },
    {
        "city_cn": "湖北省恩施州",
        "host": "116.210.168.73",
        "port": "43780"
    },
    {
        "city_cn": "安徽省六安市",
        "host": "183.165.224.23",
        "port": "55250"
    },
    {
        "city_cn": "山东省",
        "host": "140.250.89.38",
        "port": "44094"
    },
    {
        "city_cn": "福建省漳州市",
        "host": "27.157.89.29",
        "port": "55449"
    },
    {
        "city_cn": "江苏省南通市",
        "host": "114.231.45.189",
        "port": "31025"
    },
    {
        "city_cn": "湖北省咸宁市",
        "host": "111.179.79.99",
        "port": "56731"
    },
    {
        "city_cn": "重庆市",
        "host": "125.87.83.106",
        "port": "58676"
    },
    {
        "city_cn": "湖北省武汉市",
        "host": "119.98.220.63",
        "port": "45987"
    }
]
# os.environ['NO_PROXY'] = "httpbin.org"
dip = random.choice(result)
host = dip['host']
port = dip['port']
def proxy():
    ip = "42.59.116.54:40013"
    # ip = host + ":" + port
    proxies = {
        'HTTP': 'HTTP://' + ip,
        'HTTPS': 'HTTPS://' + ip,
    }
    print(proxies)
    url = 'https://httpbin.org/ip'
    content = requests.get(url=url, proxies=proxies).text
    print(content)


if __name__ == '__main__':
    proxy()
    
    # proxy = {
    #     "http": "222.89.70.65:25065",
    # }
