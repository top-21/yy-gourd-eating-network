import requests
import re
import json
from lxml import etree


class SpiderIP:
    def __init__(self):
        self.__headers = {
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
        }

    # 快代理
    def KDL(self) -> list:
        SpiderUrl = "https://www.kuaidaili.com/free/"
        try:
            r = requests.get(url=SpiderUrl,headers=self.__headers).text

            re_res = re.findall(r"fpsList = (.*?);",r,re.S)
            IPdata = json.loads(re_res[0])
            return IPdata
        except Exception as e:
            return []

    # 极光免费代理
    def JGDL(self, Page=1):
        SpiderUrl = f"https://ip.jghttp.com/{Page}/"
        try:
            response = requests.get(SpiderUrl,headers=self.__headers)
            response.encoding = 'utf-8'
            etree_html = etree.HTML(response.text)
            divs = etree_html.xpath('/html/body/div[2]/div/div/div[1]/div[2]/div/div')
            IP_List = []
            for obj in divs:
                className = obj.xpath("./@class")
                if len(className) > 0:
                    if className[0] != "th":
                        IP = obj.xpath('./div/div[1]/text()')[0]
                        PORT = obj.xpath('./div/div[2]/text()')[0]
                        TYPE = obj.xpath('./div/div[3]/text()')[0]
                        GPS = obj.xpath('./div/div[4]/text()')[0]
                        IpInfo = {
                            "IP": IP,
                            "PORT": PORT,
                            "TYPE": TYPE,
                            "GPS": GPS
                        }
                        IP_List.append(IpInfo)

            return IP_List
        except Exception as e:
            return []

    # 太阳HTTP免费代理
    def TYDL(self, Page=1) -> list:
        SpiderUrl = f"http://ip.tyhttp.com/{Page}/"
        try:
            response = requests.get(SpiderUrl,headers=self.__headers)
            response.encoding = 'utf-8'
            etree_html = etree.HTML(response.text)
            divs = etree_html.xpath('/html/body/div[2]/div/div/div[1]/div[2]/div/div')
            IP_List = []
            for obj in divs:
                className = obj.xpath("./@class")
                if len(className) > 0:
                    if className[0] == "list":
                        IP = obj.xpath('./div/div[1]/text()')[0]
                        PORT = obj.xpath('./div/div[2]/text()')[0]
                        TYPE = obj.xpath('./div/div[3]/text()')[0]
                        GPS = obj.xpath('./div/div[4]/text()')[0]
                        IpInfo = {
                            "IP": IP,
                            "PORT": PORT,
                            "TYPE": TYPE,
                            "GPS": GPS
                        }
                        IP_List.append(IpInfo)
            return IP_List
        except Exception as e:
            return []

    # 89免费代理
    def BJDL(self):
        SpiderUrl = "https://www.89ip.cn/index_1.html"
        response = requests.get(SpiderUrl,headers=self.__headers).text
        etree_html = etree.HTML(response)

        tr_list = etree_html.xpath('/html/body/div[3]/div[1]/div/div[1]/table/tbody/tr')

        for obj in tr_list:
            IP = str(obj.xpath('./td[1]/text()')[0]).strip()
            PORT = str(obj.xpath('./td[2]/text()')[0]).strip()

if __name__ == '__main__':
    sp = SpiderIP()

    sp.BJDL()
