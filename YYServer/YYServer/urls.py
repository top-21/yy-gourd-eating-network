
from api.utils.sex import SexSpider
from api.utils.haijiao import HaiJiaoSpider
from django.urls import path

urlpatterns = [
    # 66吃瓜网
    path('sex/v1/plater/', SexSpider.GetPlatesList),    # 获取板块
    path('sex/v1/plater/info/', SexSpider.GetPlatesInfo),    # 获取板块
    path('sex/v1/info/test/', SexSpider.GetChiGuaInfo),    # 获取内容详情
    path('sex/v1/search/', SexSpider.GetSearchInfo),    # 获取搜索内容
    path('sex/v1/type/info/', SexSpider.GetTypeInfo),    # 获取分类内容
    
    # 海角社区
    path('haijiao/v1/card/list/', HaiJiaoSpider.GetCardList),   # 帖子列表
    path('haijiao/v1/card/info/', HaiJiaoSpider.GetCardInfo),   # 帖子详情
    path('haijiao/v1/card/Commit/', HaiJiaoSpider.GetCardCommit),   # 帖子评论
    path('haijiao/v1/SearchText/', HaiJiaoSpider.GetHaiJiaoCardSearchText),   # 搜索帖子
    path('haijiao/v1/Search/id/video/', HaiJiaoSpider.GetHaiJiaoMFVideo),   # 根据id取免费视频
]
