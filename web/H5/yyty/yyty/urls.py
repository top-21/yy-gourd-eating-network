
from django.contrib import admin
from django.urls import path

from web.utlis import web
urlpatterns = [
    path('yyty/index/', web.index),  # 首页
    path('yyty/news/info/', web.news_info), # 新闻详情
    path('yyty/news/search/', web.news_search), # 搜索
]
