 function p(t,a) {
    $.ajax({
        headers: {},
        type: "GET",
        contentType: " text/plain",
        url: t,
        dataType: "text",
        success: function(i) {
            let o = (new A).decode(i);
            if (a !=undefined) {
                a.src = o;
            }
            return o;
        }
    })
}
function A() {
    var e = "ABCD*EFGHIJKLMNOPQRSTUVWX#YZabcdefghijklmnopqrstuvwxyz1234567890"
      , t = (this.encode = function(a) {
        var n, s, i, o, _, r, d = "", c = 0;
        for (a = t(a); c < a.length; )
            i = (n = a.charCodeAt(c++)) >> 2,
            o = (3 & n) << 4 | (n = a.charCodeAt(c++)) >> 4,
            _ = (15 & n) << 2 | (s = a.charCodeAt(c++)) >> 6,
            r = 63 & s,
            isNaN(n) ? _ = r = 64 : isNaN(s) && (r = 64),
            d = d + e.charAt(i) + e.charAt(o) + e.charAt(_) + e.charAt(r);
        return d
    }
    ,
    this.decode = function(t) {
        var n, s, i, o, _, r, d = "", c = 0;
        for (t = t.replace(/[^A-Za-z0-9\*\#]/g, ""); c < t.length; )
            i = e.indexOf(t.charAt(c++)),
            n = (15 & (o = e.indexOf(t.charAt(c++)))) << 4 | (_ = e.indexOf(t.charAt(c++))) >> 2,
            s = (3 & _) << 6 | (r = e.indexOf(t.charAt(c++))),
            d += String.fromCharCode(i << 2 | o >> 4),
            64 != _ && (d += String.fromCharCode(n)),
            64 != r && (d += String.fromCharCode(s));
        return a(d)
    }
    ,
    function(e) {
        e = e.replace(/\r\n/g, "\n");
        for (var t = "", a = 0; a < e.length; a++) {
            var n = e.charCodeAt(a);
            n < 128 ? t += String.fromCharCode(n) : t = 127 < n && n < 2048 ? (t += String.fromCharCode(n >> 6 | 192)) + String.fromCharCode(63 & n | 128) : (t = (t += String.fromCharCode(n >> 12 | 224)) + String.fromCharCode(n >> 6 & 63 | 128)) + String.fromCharCode(63 & n | 128)
        }
        return t
    }
    )
      , a = function(e) {
        for (var t, a, n = "", s = 0, i = 0; s < e.length; )
            (t = e.charCodeAt(s)) < 128 ? (n += String.fromCharCode(t),
            s++) : 191 < t && t < 224 ? (i = e.charCodeAt(s + 1),
            n += String.fromCharCode((31 & t) << 6 | 63 & i),
            s += 2) : (i = e.charCodeAt(s + 1),
            a = e.charCodeAt(s + 2),
            n += String.fromCharCode((15 & t) << 12 | (63 & i) << 6 | 63 & a),
            s += 3);
        return n
    }
}

// 生成随机数
class UniqueRandomGenerator {
    constructor(min, max) {
        this.min = min;
        this.max = max;
        this.usedNumbers = new Set();
    }

    generateUniqueRandom() {
        let randomNum;
        do {
            randomNum = Math.floor(Math.random() * (this.max - this.min + 1)) + this.min;
        } while (this.usedNumbers.has(randomNum));

        this.usedNumbers.add(randomNum);

        // 如果所有可能的数字都已经使用过，则需要重置已使用的数字集合  
        if (this.usedNumbers.size === (this.max - this.min + 1)) {
            this.usedNumbers.clear();
        }

        return randomNum;
    }
}